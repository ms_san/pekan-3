<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <div id="app">

    <input type="text" v-model="username">
    <button @click="addUser" v-show="!update">Add</button>
    <button @click="updateUser" v-show="update">update</button>

    <ul>
      <li v-for="(user, index) in users">
        {{ user.name }} 
        ||
        <button @click="editUser(user, index)">Edit</button>
        ||
        <button @click="deleteUser(index)">Delete</button>
      </li>
    </ul>
  </div>


  <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
  <script>
    var app = new Vue({
      el: '#app',
      data: {
        users: [
        ],
        updateName : false,
        username : '',
        selectUserId : null,
      },
      mounted(){
        axios.get('api/welcome').then(response => this.users = response.data);
      },
      methods: {
        addUser: function () {
          this.users.push({
            name: this.username,
          });
          this.username=''
        },
        deleteUser: function (index) {
          if (confirm('Yakin ingin hapus ?')) {
            this.users.splice(index,1);
          }
        },
        editUser: function (user, index) {
          this.selectUserId = index
          this.update = true
          this.username = user.name
        },
       updateUser: function () {
          this.users[this.selectUserId].name = this.username
          this.username = ''
          this.update = false
          this.selectUserId = null
        },
      }
    })
  </script>
</body>
</html>