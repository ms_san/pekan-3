<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Resources\NameResource;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {
        $users = User::orderBy('created_at', 'desc')->get();

        return NameResource::collection($users);
    }

    public function store(Request $request)
    {
        $user = User::create([
            'name' => $request->name
        ]);

        return $user;
    }
}
